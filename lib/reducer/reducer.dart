import 'package:demo_app/action/action.dart';
import 'package:demo_app/store/app_state.dart';
import 'package:redux/redux.dart';

AppState appReducer(AppState state, action) {
  return appCombineReducer(state, action);
}

Reducer<AppState> appCombineReducer = combineReducers<AppState>(
  [
    updateCounterReducer,
    updateCounter2Reducer,
  ],
);

final updateCounterReducer =
    TypedReducer<AppState, UpdateCounterAction>(_updateCounterReducer);
AppState _updateCounterReducer(AppState appState, UpdateCounterAction action) {
  return appState.rebuild((updates) => updates..counter = action.counter);
}

final updateCounter2Reducer =
    TypedReducer<AppState, UpdateCounter2Action>(_updateCounter2Reducer);
AppState _updateCounter2Reducer(
    AppState appState, UpdateCounter2Action action) {
  return appState.rebuild((updates) => updates..counter2 = action.counter2);
}
