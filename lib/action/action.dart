import 'package:built_value/built_value.dart';

part 'action.g.dart';

abstract class UpdateCounterAction
    implements Built<UpdateCounterAction, UpdateCounterActionBuilder> {
  int get counter;
  UpdateCounterAction._();
  factory UpdateCounterAction(
          [void Function(UpdateCounterActionBuilder) updates]) =
      _$UpdateCounterAction;
}

abstract class UpdateCounter2Action
    implements Built<UpdateCounter2Action, UpdateCounter2ActionBuilder> {
  int get counter2;
  UpdateCounter2Action._();
  factory UpdateCounter2Action(
          [void Function(UpdateCounter2ActionBuilder) updates]) =
      _$UpdateCounter2Action;
}
