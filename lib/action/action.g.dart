// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'action.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$UpdateCounterAction extends UpdateCounterAction {
  @override
  final int counter;

  factory _$UpdateCounterAction(
          [void Function(UpdateCounterActionBuilder)? updates]) =>
      (new UpdateCounterActionBuilder()..update(updates)).build();

  _$UpdateCounterAction._({required this.counter}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        counter, 'UpdateCounterAction', 'counter');
  }

  @override
  UpdateCounterAction rebuild(
          void Function(UpdateCounterActionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UpdateCounterActionBuilder toBuilder() =>
      new UpdateCounterActionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UpdateCounterAction && counter == other.counter;
  }

  @override
  int get hashCode {
    return $jf($jc(0, counter.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UpdateCounterAction')
          ..add('counter', counter))
        .toString();
  }
}

class UpdateCounterActionBuilder
    implements Builder<UpdateCounterAction, UpdateCounterActionBuilder> {
  _$UpdateCounterAction? _$v;

  int? _counter;
  int? get counter => _$this._counter;
  set counter(int? counter) => _$this._counter = counter;

  UpdateCounterActionBuilder();

  UpdateCounterActionBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _counter = $v.counter;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UpdateCounterAction other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$UpdateCounterAction;
  }

  @override
  void update(void Function(UpdateCounterActionBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$UpdateCounterAction build() {
    final _$result = _$v ??
        new _$UpdateCounterAction._(
            counter: BuiltValueNullFieldError.checkNotNull(
                counter, 'UpdateCounterAction', 'counter'));
    replace(_$result);
    return _$result;
  }
}

class _$UpdateCounter2Action extends UpdateCounter2Action {
  @override
  final int counter2;

  factory _$UpdateCounter2Action(
          [void Function(UpdateCounter2ActionBuilder)? updates]) =>
      (new UpdateCounter2ActionBuilder()..update(updates)).build();

  _$UpdateCounter2Action._({required this.counter2}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        counter2, 'UpdateCounter2Action', 'counter2');
  }

  @override
  UpdateCounter2Action rebuild(
          void Function(UpdateCounter2ActionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UpdateCounter2ActionBuilder toBuilder() =>
      new UpdateCounter2ActionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UpdateCounter2Action && counter2 == other.counter2;
  }

  @override
  int get hashCode {
    return $jf($jc(0, counter2.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UpdateCounter2Action')
          ..add('counter2', counter2))
        .toString();
  }
}

class UpdateCounter2ActionBuilder
    implements Builder<UpdateCounter2Action, UpdateCounter2ActionBuilder> {
  _$UpdateCounter2Action? _$v;

  int? _counter2;
  int? get counter2 => _$this._counter2;
  set counter2(int? counter2) => _$this._counter2 = counter2;

  UpdateCounter2ActionBuilder();

  UpdateCounter2ActionBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _counter2 = $v.counter2;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UpdateCounter2Action other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$UpdateCounter2Action;
  }

  @override
  void update(void Function(UpdateCounter2ActionBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$UpdateCounter2Action build() {
    final _$result = _$v ??
        new _$UpdateCounter2Action._(
            counter2: BuiltValueNullFieldError.checkNotNull(
                counter2, 'UpdateCounter2Action', 'counter2'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
