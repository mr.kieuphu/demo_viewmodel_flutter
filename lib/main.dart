import 'package:demo_app/di/injection.dart';
import 'package:demo_app/reducer/reducer.dart';
import 'package:demo_app/store/app_state.dart';
import 'package:demo_app/view_model/home_view_logic.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

import 'home.dart';

void main() {
  configureDepencies();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final Store<AppState> store = Store<AppState>(
      appReducer,
      initialState: AppState.initialState(),
    );
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MyHomePage(
          title: 'Flutter Demo Home Page',
          viewLogic: getIt<HomeViewLogic>(instanceName: "Main"),
        ),
      ),
    );
  }
}
