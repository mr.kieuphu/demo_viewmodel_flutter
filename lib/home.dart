import 'package:demo_app/view_model/home_view_logic.dart';
import 'package:demo_app/view_model/home_vm.dart';
import 'package:demo_app/store/app_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title, required this.viewLogic})
      : super(key: key);

  final String title;
  final HomeViewLogic viewLogic;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    final store = StoreProvider.of<AppState>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            StoreConnector<AppState, HomeViewModel>(
              onInit: (store) {},
              converter: (store) => widget.viewLogic.apply(store),
              distinct: true,
              builder: (context, model) =>
                  MyTextWidget(text: model.counterString),
            ),
            TextButton(
                onPressed: () {
                  widget.viewLogic.actionButtonOnPressed(context);
                },
                child: const Text("Next")),
          ],
        ),
      ),
      floatingActionButton: MyButtonWidget(
        onPressed: () {
          widget.viewLogic.floatingButtonOnPressed(store);
        },
      ),
    );
  }
}

class MyTextWidget extends StatelessWidget {
  const MyTextWidget({Key? key, required this.text}) : super(key: key);
  final String text;

  @override
  Widget build(BuildContext context) {
    print("My text build");
    return Text(
      text,
      style: Theme.of(context).textTheme.headline4,
    );
  }
}

class MyButtonWidget extends StatelessWidget {
  const MyButtonWidget({Key? key, this.onPressed}) : super(key: key);
  final Function()? onPressed;

  @override
  Widget build(BuildContext context) {
    print("My button buid");
    return FloatingActionButton(
      onPressed: onPressed,
      tooltip: 'Increment',
      child: const Icon(Icons.add),
    );
  }
}
