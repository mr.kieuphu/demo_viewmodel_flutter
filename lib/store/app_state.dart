import 'package:built_value/built_value.dart';

part 'app_state.g.dart';

abstract class AppState implements Built<AppState, AppStateBuilder> {
  int? get counter;
  int? get counter2;
  factory AppState.initialState() {
    return AppState((updates) => updates
      ..counter = 0
      ..counter2 = 0);
  }
  AppState._();
  factory AppState([void Function(AppStateBuilder) updates]) = _$AppState;
}
