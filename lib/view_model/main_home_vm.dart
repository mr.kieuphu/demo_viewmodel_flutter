import 'package:demo_app/action/action.dart';
import 'package:demo_app/di/injection.dart';
import 'package:demo_app/home.dart';
import 'package:demo_app/store/app_state.dart';
import 'package:demo_app/view_model/home_view_logic.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:redux/redux.dart';

import 'home_vm.dart';

@Named("Main")
@Injectable(as: HomeViewLogic)
class MainHomeViewLogic implements HomeViewLogic {
  @override
  HomeViewModel apply(Store<AppState> store) {
    return HomeViewModel((updates) =>
        updates..counterString = "${store.state.counter ?? 0} - hihihi");
  }

  @override
  void floatingButtonOnPressed(Store<AppState> store) {
    store.dispatch(UpdateCounterAction(
        (updates) => updates..counter = (store.state.counter ?? 0) + 1));
  }

  @override
  void actionButtonOnPressed(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => MyHomePage(
            title: "Second",
            viewLogic: getIt<HomeViewLogic>(instanceName: "Second")),
      ),
    );
  }
}
