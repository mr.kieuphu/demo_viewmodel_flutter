import 'package:demo_app/store/app_state.dart';
import 'package:demo_app/view_model/home_vm.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

abstract class HomeViewLogic {
  void floatingButtonOnPressed(Store<AppState> store);
  void actionButtonOnPressed(BuildContext context);
  HomeViewModel apply(Store<AppState> store);
}
