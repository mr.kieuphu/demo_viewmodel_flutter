// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_vm.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$HomeViewModel extends HomeViewModel {
  @override
  final String counterString;

  factory _$HomeViewModel([void Function(HomeViewModelBuilder)? updates]) =>
      (new HomeViewModelBuilder()..update(updates)).build();

  _$HomeViewModel._({required this.counterString}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        counterString, 'HomeViewModel', 'counterString');
  }

  @override
  HomeViewModel rebuild(void Function(HomeViewModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  HomeViewModelBuilder toBuilder() => new HomeViewModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is HomeViewModel && counterString == other.counterString;
  }

  @override
  int get hashCode {
    return $jf($jc(0, counterString.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('HomeViewModel')
          ..add('counterString', counterString))
        .toString();
  }
}

class HomeViewModelBuilder
    implements Builder<HomeViewModel, HomeViewModelBuilder> {
  _$HomeViewModel? _$v;

  String? _counterString;
  String? get counterString => _$this._counterString;
  set counterString(String? counterString) =>
      _$this._counterString = counterString;

  HomeViewModelBuilder();

  HomeViewModelBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _counterString = $v.counterString;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(HomeViewModel other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$HomeViewModel;
  }

  @override
  void update(void Function(HomeViewModelBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$HomeViewModel build() {
    final _$result = _$v ??
        new _$HomeViewModel._(
            counterString: BuiltValueNullFieldError.checkNotNull(
                counterString, 'HomeViewModel', 'counterString'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
