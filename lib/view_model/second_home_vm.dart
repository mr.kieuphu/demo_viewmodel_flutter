import 'package:demo_app/action/action.dart';
import 'package:demo_app/di/injection.dart';
import 'package:demo_app/home.dart';
import 'package:demo_app/store/app_state.dart';
import 'package:demo_app/view_model/home_view_logic.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:redux/redux.dart';
import 'home_vm.dart';

@Named("Second")
@Injectable(as: HomeViewLogic)
class SecondHomeViewLogic implements HomeViewLogic {
  @override
  HomeViewModel apply(Store<AppState> store) {
    return HomeViewModel((updates) =>
        updates..counterString = "${store.state.counter2 ?? 0} - hahaha");
  }

  @override
  void floatingButtonOnPressed(Store<AppState> store) {
    store.dispatch(UpdateCounter2Action(
        (updates) => updates..counter2 = (store.state.counter2 ?? 0) + 2));
  }

  @override
  void actionButtonOnPressed(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => MyHomePage(
            title: "Main",
            viewLogic: getIt<HomeViewLogic>(instanceName: "Main")),
      ),
    );
  }
}
