import 'package:built_value/built_value.dart';

part 'home_vm.g.dart';

abstract class HomeViewModel
    implements Built<HomeViewModel, HomeViewModelBuilder> {
  String get counterString;
  HomeViewModel._();
  factory HomeViewModel([void Function(HomeViewModelBuilder) updates]) =
      _$HomeViewModel;
}
